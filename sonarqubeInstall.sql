--Create database with CS and AS collation options
drop database sonar
create DATABASE sonar
COLLATE SQL_Latin1_General_CP1_CS_AS;  
SELECT name, collation_name FROM sys.databases;

--
SELECT is_read_committed_snapshot_on FROM sys.databases WHERE name='sonar';
ALTER DATABASE sonar SET READ_COMMITTED_SNAPSHOT ON WITH ROLLBACK IMMEDIATE;

--Create scheema
USE sonar;
create SCHEMA sonar

-- Create Login and user
USE sonar;
CREATE LOGIN sonaradmin  
    WITH PASSWORD = 'Fistik02!';
   
CREATE USER sonaradmin FOR LOGIN sonaradmin;

-- Add user to db_owner group
EXEC sp_addrolemember 'db_owner', sonaradmin